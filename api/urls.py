from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('studentapi/', views.StudentLC.as_view()),
    path('studentapi/<int:pk>', views.StudentRUD.as_view()),

]
